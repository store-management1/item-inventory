package com.stm.iteminventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan("com.stm.iteminventory")
@EnableJpaRepositories
@EnableAutoConfiguration
@EnableJpaAuditing
public class StmItemInventoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(StmItemInventoryApplication.class, args);
	}

}
